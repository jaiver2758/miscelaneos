import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {
  // ElementRef = con este podemos manipular html
  constructor(private elementRef: ElementRef) {
    console.log('Directiva llamada');
  }
  // input es para decir que algo (Variable) viene de afuera
  @Input("appResaltado") nuevoColor: string;
  // @hostListener es el que esta pendiente o escuha de lo que sucede
  // en el html y funciona asi ...
  @HostListener('mouseenter') mouseEntro() {

    this.resaltar(this.nuevoColor || 'yellow');
    // console.log(this.nuevoColor);

    // this.elementRef.nativeElement.style.backgroundColor = "yellow";
  }
  @HostListener('mouseleave') mouseSalio() {
    this.resaltar(null);
  }

  private resaltar(color: string) {
    this.elementRef.nativeElement.style.backgroundColor = color;

  }
}
